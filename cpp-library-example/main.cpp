/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <clutter/clutter.h>

#include "CppLibraryExample.h"

int
main (int argc, char **argv)
{
  /* FIXME: We want to use GApplication.
   * https://phabricator.apertis.org/T3212 */

  if (clutter_init (0, NULL) != CLUTTER_INIT_SUCCESS)
    {
      g_error ("Failed to initialize Clutter");
      return -1;
    }

  CppLibraryExample example = CppLibraryExample ();

  clutter_main ();

  return 0;
}
