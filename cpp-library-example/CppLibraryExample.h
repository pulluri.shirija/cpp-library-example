/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SRC_CPPLIBRARYEXAMPLE_H__
#define SRC_CPPLIBRARYEXAMPLE_H__

#include <clutter/clutter.h>

class CppLibraryExample
{
  ClutterActor *stage;
 public:
  CppLibraryExample ();
  ~CppLibraryExample ();
  void create_cat_actors ();
};

#endif  // SRC_CPPLIBRARYEXAMPLE_H__
