/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib-object.h>

#include <gdk/gdk.h>
#include <clutter/clutter.h>
#include <clutter/gdk/clutter-gdk.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#include <mildenhall/mildenhall.h>
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wredundant-decls"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#pragma GCC diagnostic pop

#include "CppLibraryExample.h"

using cv::Mat;
using std::vector;

static ClutterActor *
get_actor_from_pixbuf (GdkPixbuf *pixbuf)
{
  GError *err = NULL;
  ClutterActor * image_actor;
  ClutterContent *image = clutter_image_new ();
  clutter_image_set_data (CLUTTER_IMAGE (image),
                          gdk_pixbuf_get_pixels (pixbuf),
                          gdk_pixbuf_get_has_alpha (pixbuf)
                            ? COGL_PIXEL_FORMAT_RGBA_8888
                            : COGL_PIXEL_FORMAT_RGB_888,
                          gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf),
                          gdk_pixbuf_get_rowstride (pixbuf),
                          &err);

  if (err)
  {
    g_error ("Unable to load pixbuf: %s", err->message);
    g_clear_error (&err);
    return NULL;
  }

  image_actor = clutter_actor_new ();
  clutter_actor_set_content (image_actor, image);
  clutter_actor_set_size (image_actor,
                          gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf));
  g_object_unref (image);

  return image_actor;
}

static ClutterActor *
get_actor_from_opencv_color_image (const Mat& image)
{
  /* FIXME: We can get rid of using GdkPixbuf.
   * https://phabricator.apertis.org/T3211 */
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_data (
              image.data,
              GDK_COLORSPACE_RGB,
              false,
              8,
              image.cols,
              image.rows,
              image.step,
              NULL,
              NULL);
  return get_actor_from_pixbuf (pixbuf);
}

static gboolean
toggle_filter_cb (ClutterActor *actor,
                  ClutterEvent *event,
                  ClutterActor *filtered_actor)
{
  if (!clutter_actor_get_paint_visibility (filtered_actor))
    clutter_actor_show (filtered_actor);
  else
    clutter_actor_hide (filtered_actor);
  return TRUE;
}

CppLibraryExample::CppLibraryExample ()
{
  stage = mildenhall_stage_new (BUNDLE_ID);

  clutter_actor_show (stage);

  clutter_actor_set_layout_manager (stage,
      clutter_bin_layout_new (CLUTTER_BIN_ALIGNMENT_CENTER,
                              CLUTTER_BIN_ALIGNMENT_CENTER));

  create_cat_actors ();
}

CppLibraryExample::~CppLibraryExample ()
{
  clutter_actor_destroy (stage);
}

void CppLibraryExample::create_cat_actors ()
{
  /* Init cats */
  GError *err = NULL;
 
  /* FIXME: We can get rid of using GdkPixbuf.
   * https://phabricator.apertis.org/T3211 */
  GdkPixbuf *cat_pixbuf = gdk_pixbuf_new_from_resource (
    "/org/apertis/Examples/CppLibrary/cat.jpg", &err);

  if (err)
  {
    g_error ("Unable to load image: %s", err->message);
    g_clear_error (&err);
    return;
  }

  Mat opencv_image (
    cv::Size (gdk_pixbuf_get_width (cat_pixbuf),
              gdk_pixbuf_get_height (cat_pixbuf)),
    CV_8UC3,
    gdk_pixbuf_get_pixels (cat_pixbuf),
    gdk_pixbuf_get_rowstride (cat_pixbuf));

  if (opencv_image.empty ()) {
    g_error ("Error converting to OpenCV image.");
    return;
  }

  Mat filtered;
  Canny (opencv_image, filtered, 50, 200, 3);
  cvtColor (filtered, filtered, CV_GRAY2RGB);

  ClutterActor *cat_actor, *cat_filtered_actor;
  cat_filtered_actor = get_actor_from_opencv_color_image (filtered);
  cat_actor = get_actor_from_pixbuf (cat_pixbuf);

  g_object_unref (cat_pixbuf);

  clutter_actor_add_child (stage, cat_actor);
  clutter_actor_add_child (stage, cat_filtered_actor);

  g_signal_connect (stage, "button-press-event",
    G_CALLBACK (toggle_filter_cb), cat_filtered_actor);
}
